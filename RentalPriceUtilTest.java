package com.gem.dto;

import com.gem.dto.constant.RentalPriceConst;
import com.gem.dto.util.RentalPriceUtil;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class RentalPriceUtilTest {
    LocalTime evening = LocalTime.of(17,0);
    LocalTime morning = LocalTime.of(8,0);
    LocalDateTime start = LocalDate.of(2021, Month.AUGUST, 1).atTime(morning);

    LocalDate Mon = LocalDate.of(2021, Month.SEPTEMBER, 20);
    LocalDate Tue = LocalDate.of(2021, Month.SEPTEMBER, 21);
    LocalDate Wed = LocalDate.of(2021, Month.SEPTEMBER, 22);
    LocalDate Thu = LocalDate.of(2021, Month.SEPTEMBER, 23);
    LocalDate Fri = LocalDate.of(2021, Month.SEPTEMBER, 24);
    LocalDate Sat = LocalDate.of(2021, Month.SEPTEMBER, 25);
    LocalDate Sun = LocalDate.of(2021, Month.SEPTEMBER, 26);
    LocalDate NextMon = LocalDate.of(2021, Month.SEPTEMBER, 27);

    RentalPriceQtyDTO flat = new RentalPriceQtyDTO("flat", BigDecimal.valueOf(40), null, null);
    RentalPriceQtyDTO specialFlat = new RentalPriceQtyDTO("specialFlat", BigDecimal.valueOf(40), null, null);
    RentalPriceQtyDTO specialFlatWithLimit = new RentalPriceQtyDTO("specialFlatWithLimit", BigDecimal.valueOf(40), 25, null);
    RentalPriceQtyDTO specialMonthWithoutLimit = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Day, BigDecimal.valueOf(40), null, 30);
    RentalPriceQtyDTO specialMonthWithLengthAndLimit = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Day, BigDecimal.valueOf(40), 25, 30);
    RentalPriceQtyDTO dayRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Day, BigDecimal.valueOf(225), 8, 1);
    RentalPriceQtyDTO weeklyRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Day, BigDecimal.valueOf(900), 40, 7);
    RentalPriceQtyDTO monthlyRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Day, BigDecimal.valueOf(2700), 160, 30);
    RentalPriceQtyDTO weekendRate = new RentalPriceQtyDTO(DayOfWeek.FRIDAY, DayOfWeek.MONDAY, BigDecimal.valueOf(225/8*12), 2);
    RentalPriceQtyDTO fiveDayRate = new RentalPriceQtyDTO(DayOfWeek.MONDAY, DayOfWeek.FRIDAY, BigDecimal.valueOf(800), 200);
    RentalPriceQtyDTO hourlyRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(40), 10, 1);
    RentalPriceQtyDTO specialTwoMonths = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Day, BigDecimal.valueOf(5000), 25, 60);
    RentalPriceQtyDTO specialTwoWeeks = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Day, BigDecimal.valueOf(1700), 25, 15);
    RentalPriceQtyDTO specialSixMonths = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Day, BigDecimal.valueOf(14000), 25, 180);

    RentalPriceQtyDTO fourHourRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(150), null, 4);
    RentalPriceQtyDTO eightHourRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(220), null, 8);

    InvoiceRentUnitDTO line = new InvoiceRentUnitDTO();

    @Before
    public void setup() {
        weekendRate.startTime = LocalTime.of(17,0);
        weekendRate.endTime = LocalTime.of(9,0);

        fiveDayRate.startTime = LocalTime.of(8,0);
        fiveDayRate.endTime = LocalTime.of(18,0);
    }

    @Test
    public void testCalculateRates_WestSide_MinimumFour() {

        line.rentalPriceQtyDTOs = Arrays.asList(dayRate, weekendRate, monthlyRate, weeklyRate, fourHourRate, eightHourRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(46), line);
        assertEquals(450, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(dayRate.qty, 2, 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusDays(1), line);
        assertEquals(225, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(dayRate.qty, 1, 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(9), line);
        assertEquals(225, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(dayRate.qty, 1, 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(8), line);
        assertEquals(220, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(eightHourRate.qty, 1, 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(6), line);
        assertEquals(220, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(eightHourRate.qty, 1, 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(4), line);
        assertEquals(150, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(fourHourRate.qty, 1, 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(1), line);
        assertEquals(150, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(fourHourRate.qty, 1, 0.01);
    }

    @Test
    public void testCalculateRates_WestSide_Weekend() {
        RentalPriceQtyDTO wk1 = new RentalPriceQtyDTO(DayOfWeek.FRIDAY, DayOfWeek.MONDAY, BigDecimal.valueOf(225*2), null);
        wk1.startTime = LocalTime.of(12,0);
        wk1.endTime = LocalTime.of(9,0);

        RentalPriceQtyDTO wk2 = new RentalPriceQtyDTO(DayOfWeek.SATURDAY, DayOfWeek.MONDAY, BigDecimal.valueOf(225*1.5), null);
        wk2.startTime = LocalTime.of(9,0);
        wk2.endTime = LocalTime.of(9,0);

        RentalPriceQtyDTO wk3 = new RentalPriceQtyDTO(DayOfWeek.SATURDAY, DayOfWeek.MONDAY, BigDecimal.valueOf(225), null);
        wk3.startTime = LocalTime.of(17,0);
        wk3.endTime = LocalTime.of(9,0);

        line.rentalPriceQtyDTOs = Arrays.asList(dayRate, wk1, wk2, wk3, monthlyRate, weeklyRate, fourHourRate, eightHourRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(Fri.atTime(16,0), NextMon.atTime(9,0), line);
        assertEquals(1, wk1.qty, 0.01);
        //assertEquals(1, fourHourRate.qty, 0.01);
        //assertEquals(150+225, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(Fri.atTime(17,0), NextMon.atTime(9,0), line);
        assertEquals(1, wk1.qty, 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(Sat.atTime(17,0), NextMon.atTime(9,0), line);
        assertEquals(1, wk3.qty, 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(Sat.atTime(13,0), NextMon.atTime(9,0), line);
        assertEquals(1, wk2.qty, 0.01);
    }

    @Test
    public void testCalculateRates_WestSide_Flat() {
        RentalPriceQtyDTO fourHourRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(900), null, 4);
        RentalPriceQtyDTO eightHourRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(900), null, 8);
        RentalPriceQtyDTO dayRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Day, BigDecimal.valueOf(900), 8, 1);

        line.rentalPriceQtyDTOs = Arrays.asList(dayRate, monthlyRate, weeklyRate, fourHourRate, eightHourRate);

        LocalDateTime start = LocalDateTime.of(2022, Month.SEPTEMBER, 28, 5, 54);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(46), line);
        assertEquals(900, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusDays(1), line);
        assertEquals(900, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(9), line);
        assertEquals(900, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(8), line);
        assertEquals(900, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(6), line);
        assertEquals(900, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(4), line);
        assertEquals(900, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(1), line);
        assertEquals(900, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
    }

    @Test
    public void testCalculateRates_HoursRateMorning() {
        RentalPriceQtyDTO fourHourRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(150), null, 4);
        fourHourRate.startTime = LocalTime.of(8, 0);
        fourHourRate.endTime = LocalTime.of(16, 0);

        line.rentalPriceQtyDTOs = Arrays.asList(fourHourRate, monthlyRate);
        LocalDateTime start = LocalDateTime.of(2022, Month.SEPTEMBER, 28, 8, 54);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(4), line);
        assertEquals(150, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(1, fourHourRate.qty, 0.01);
    }

    @Test
    public void testFitStartAndEnd_NoDate() {
        RentalPriceQtyDTO fourHourRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(150), null, 4);
        fourHourRate.startTime = LocalTime.of(8, 0);
        fourHourRate.endTime = LocalTime.of(16, 0);

        LocalDateTime startTooEarly = LocalDateTime.of(2022, Month.SEPTEMBER, 28, 7, 59);
        LocalDateTime endTooLate = LocalDateTime.of(2022, Month.SEPTEMBER, 28, 16, 1);
        LocalDate anyDay = LocalDate.of(2022, Month.SEPTEMBER, 28);

        assertFalse("start too early", RentalPriceUtil.fitStartAndEnd(fourHourRate, startTooEarly, startTooEarly.plusHours(3)));
        assertTrue("end too late is fine", RentalPriceUtil.fitStartAndEnd(fourHourRate, endTooLate.minusHours(3), endTooLate));
        assertTrue("too long", RentalPriceUtil.fitStartAndEnd(fourHourRate, anyDay.atTime(fourHourRate.startTime), anyDay.atTime(fourHourRate.endTime)));
        assertTrue("ok", RentalPriceUtil.fitStartAndEnd(fourHourRate, anyDay.atTime(LocalTime.of(12, 0)), anyDay.atTime(LocalTime.of(16, 0))));
    }

    @Test
    public void testFitStartAndEnd_WithDate() {
        RentalPriceQtyDTO MonToWed = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(150), null, 24);
        MonToWed.startTime = LocalTime.of(8, 0);
        MonToWed.endTime = LocalTime.of(17, 0);
        MonToWed.startDayOfWeek = DayOfWeek.MONDAY;
        MonToWed.endDayOfWeek = DayOfWeek.WEDNESDAY;

        LocalDate Sun = LocalDate.of(2022, Month.NOVEMBER, 6);
        LocalDate Mon = LocalDate.of(2022, Month.NOVEMBER, 7);
        LocalDate Tue = LocalDate.of(2022, Month.NOVEMBER, 8);
        LocalDate Wed = LocalDate.of(2022, Month.NOVEMBER, 9);
        LocalDate Thu = LocalDate.of(2022, Month.NOVEMBER, 10);

        assertFalse("start too early", RentalPriceUtil.fitStartAndEnd(MonToWed, Sun.atTime(9,0), Mon.atTime(10,9)));
        assertFalse("start too late", RentalPriceUtil.fitStartAndEnd(MonToWed, Wed.atTime(17,0), Thu.atTime(10,9)));

        assertTrue("end too late is fine", RentalPriceUtil.fitStartAndEnd(MonToWed, Mon.atTime(10,0), Wed.atTime(7,0)));
        assertTrue("too long", RentalPriceUtil.fitStartAndEnd(MonToWed, Mon.atTime(9,0), Tue.atTime(10,0)));
        assertTrue(RentalPriceUtil.fitStartAndEnd(MonToWed, Mon.atTime(10,0), Tue.atTime(10,0)));


        assertTrue("oneday", RentalPriceUtil.fitStartAndEnd(weekendRate, Sat.atStartOfDay(), Sun.atStartOfDay().minusHours(8)));
        assertTrue("fri", RentalPriceUtil.fitStartAndEnd(weekendRate, Fri.atTime(17,0), Sun.atTime(10,9)));
    }

    @Test
    public void testCalculateRates_HoursRateWithoutDailyRate() {
        line.rentalPriceQtyDTOs = Arrays.asList(fourHourRate, monthlyRate);
        LocalDateTime start = LocalDateTime.of(2022, Month.SEPTEMBER, 28, 5, 54);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusDays(1), line);
        assertEquals(150 * 6, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
    }

    @Test
    public void testCalculateRates_NoSolution() {
        RentalPriceQtyDTO morningOnly = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(150), null, 4);
        morningOnly.startTime = LocalTime.of(8, 0);
        morningOnly.endTime = LocalTime.of(13, 0);

        line.rentalPriceQtyDTOs = Arrays.asList(morningOnly);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusDays(1), line);
        assertEquals(0, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertNull(morningOnly.qty);
    }

    @Test
    public void testCalculateRates_CheckError() {
        RentalPriceQtyDTO dayRate = new RentalPriceQtyDTO(RentalPriceConst.DAY, BigDecimal.valueOf(200), null, 1);
        RentalPriceQtyDTO weeklyRate = new RentalPriceQtyDTO(RentalPriceConst.WEEK, BigDecimal.valueOf(800), null, 7);
        RentalPriceQtyDTO monthlyRate = new RentalPriceQtyDTO(RentalPriceConst.MONTH, BigDecimal.valueOf(2000), null, 30);
        RentalPriceQtyDTO weekendRate = new RentalPriceQtyDTO(DayOfWeek.FRIDAY, DayOfWeek.MONDAY, BigDecimal.valueOf(300), null);
        RentalPriceQtyDTO specialMonth = new RentalPriceQtyDTO("Special month", BigDecimal.valueOf(799), null, 30);
        line.rentalPriceQtyDTOs = Arrays.asList(specialMonth, dayRate, weekendRate, monthlyRate, weeklyRate);

        LocalDateTime start = LocalDateTime.of(2022, Month.SEPTEMBER, 28, 5, 54);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusDays(2), line);
        assertEquals(400, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
    }

    @Test
    public void testCalculateRates_periodTooShort() {
        RentalPriceQtyDTO hourRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(2.5), null, 1);
        RentalPriceQtyDTO dayRate = new RentalPriceQtyDTO(RentalPriceConst.DAY, BigDecimal.valueOf(20), null, 1);
        RentalPriceQtyDTO weeklyRate = new RentalPriceQtyDTO(RentalPriceConst.WEEK, BigDecimal.valueOf(80), null, 7);
        RentalPriceQtyDTO monthlyRate = new RentalPriceQtyDTO(RentalPriceConst.MONTH, BigDecimal.valueOf(240), null, 30);
        RentalPriceQtyDTO weekendRate = new RentalPriceQtyDTO(DayOfWeek.FRIDAY, DayOfWeek.MONDAY, BigDecimal.valueOf(30), null);
        line.rentalPriceQtyDTOs = Arrays.asList(hourRate, dayRate, weekendRate, monthlyRate, weeklyRate);

        LocalDateTime start = LocalDateTime.of(2022, Month.MAY, 31, 14, 31);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusMinutes(1), line);
        assertEquals(2.5, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusSeconds(2), line);
        assertEquals(2.5, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);

    }

    @Test
    public void testGetType(){
        assertEquals(RentalPriceConst.FLAT, flat.getType());

        assertEquals(RentalPriceConst.DAY, dayRate.getType());
        assertEquals(RentalPriceConst.WEEK, weeklyRate.getType());
        assertEquals(RentalPriceConst.OTHER, weekendRate.getType());
        assertEquals(RentalPriceConst.OTHER, fiveDayRate.getType());
        assertEquals(RentalPriceConst.MONTH, monthlyRate.getType());

        assertEquals(RentalPriceConst.OTHER, specialTwoMonths.getType());
        assertEquals(RentalPriceConst.OTHER, specialSixMonths.getType());

        assertEquals(RentalPriceConst.OTHER, fourHourRate.getType());

        RentalPriceQtyDTO morning = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(150), null, 4);
        morning.startTime = LocalTime.of(2,3);
        assertEquals(RentalPriceConst.OTHER, morning.getType());
    }

    @Test
    public void testCalculateRates_quantityAsInteger() {
        line.rentalPriceQtyDTOs = Arrays.asList(specialMonthWithLengthAndLimit);

        RentalPriceUtil.calculateRatesFromRentPeriod(
                start, start.plusDays(1), line);

        assertEquals(1, specialMonthWithLengthAndLimit.qty.doubleValue(), 0.01);
    }

    @Test
    public void testSelectQty() {
        assertEquals(1, RentalPriceUtil.selectQty(start, start.plusDays(1), specialMonthWithLengthAndLimit), 0.01);

        LocalDateTime start = LocalDate.of(2021, Month.SEPTEMBER, 24).atStartOfDay();
        LocalDateTime end = LocalDate.of(2022, Month.SEPTEMBER, 24).atStartOfDay().plusHours(2);
        assertEquals(3, RentalPriceUtil.selectQty(start, end, specialSixMonths), 0.01);

        assertEquals(1, RentalPriceUtil.selectQty(Fri.atTime(17,0), Fri.atStartOfDay().plusDays(1), weekendRate), 0.01);
    }

    @Test
    public void testSorting() {
        List<RentalPriceQtyDTO> list = Arrays.asList(weekendRate, dayRate, hourlyRate, monthlyRate, specialFlat, fiveDayRate,
                weeklyRate, specialSixMonths, specialTwoMonths, specialTwoWeeks);

        list.sort(new RentalPriceUtil.SortByLengthDesc());
        assertEquals(specialSixMonths, list.get(0));
    }

    @Test
    public void testCalculateRates_HourlyRate_Single() {
        line.rentalPriceQtyDTOs = Arrays.asList(hourlyRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(
                start, start.plusHours(1), line);
        assertEquals(hourlyRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(hourlyRate.meterLimit.intValue(), line.meterLimit.intValue());
        assertEquals(1, hourlyRate.qty.intValue());

        RentalPriceUtil.calculateRatesFromRentPeriod(
                start, start.plusHours(1).plusMinutes(10), line);
        assertEquals(2, hourlyRate.qty.intValue());
        assertEquals(2 * hourlyRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(2 * hourlyRate.meterLimit, line.meterLimit.intValue());
    }

    @Test
    public void testCalculateRates_HourlyRate_Multiple() {
        line.rentalPriceQtyDTOs = Arrays.asList(hourlyRate, dayRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(
                start, start.plusHours(1), line);
        assertEquals(hourlyRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(hourlyRate.meterLimit.intValue(), line.meterLimit.intValue());
        assertEquals(1, hourlyRate.qty.intValue());
        assertNull(dayRate.qty);

        RentalPriceUtil.calculateRatesFromRentPeriod(
                start, start.plusDays(3).plusHours(1).plusMinutes(10), line);
        assertEquals(2, hourlyRate.qty.intValue());
        assertEquals(3, dayRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_FlatRatesAreIgnoredWhenThereAreOthersCheaper() {
        line.rentalPriceQtyDTOs = Arrays.asList(specialFlat, hourlyRate, dayRate);
        specialFlat.rate = BigDecimal.TEN;

        RentalPriceUtil.calculateRatesFromRentPeriod(
                start, start.plusHours(1), line);
        assertEquals(hourlyRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(hourlyRate.meterLimit.intValue(), line.meterLimit.intValue());
        assertEquals(1, hourlyRate.qty.intValue());
        assertNull(dayRate.qty);
        assertNull(specialFlat.qty);

        RentalPriceUtil.calculateRatesFromRentPeriod(
                start, start.plusDays(3).plusHours(1).plusMinutes(10), line);
        assertEquals(2, hourlyRate.qty.intValue());
        assertEquals(3, dayRate.qty.intValue());
        assertNull(specialFlat.qty);
    }

    @Test
    public void testCalculateRates_FlatRatesAreIgnoredWhenThereIsAnotherCheaper() {
        line.rentalPriceQtyDTOs = Arrays.asList(specialFlat, dayRate);
        specialFlat.rate = BigDecimal.TEN;

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusDays(5), line);
        assertEquals(5, dayRate.qty.intValue());
        assertNull(specialFlat.qty);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusDays(3).plusHours(1).plusMinutes(10), line);
        assertEquals(4, dayRate.qty.intValue());
        assertNull(specialFlat.qty);
    }

    @Test
    public void testCalculateRates_NoRate() {
        line.rentalPriceQtyDTOs = new ArrayList<>();
        RentalPriceUtil.calculateRatesFromRentPeriod(
                LocalDate.of(2021, Month.AUGUST, 31).atTime(morning),
                LocalDate.of(2021, Month.SEPTEMBER, 4).atTime(evening), line);
        assertEquals(0, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(0, line.discountContracted.doubleValue(), 0.01);
        assertNull(line.meterLimit);
        assertTrue(line.rentalPriceQtyDTOs.isEmpty());
    }

    @Test
    public void testCalculateRates_SingleRate_Flat() {
        line.rentalPriceQtyDTOs = Arrays.asList(specialFlat);
        RentalPriceUtil.calculateRatesFromRentPeriod(
                LocalDate.of(2021, Month.AUGUST, 31).atTime(morning),
                LocalDate.of(2021, Month.SEPTEMBER, 4).atTime(evening), line);
        assertEquals(1, specialFlat.qty.intValue());
        assertEquals(specialFlat.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertNull(line.meterLimit);
    }

    @Test
    public void testCalculateRates_SingleRate_FlatWithLimit() {
        line.rentalPriceQtyDTOs = Arrays.asList(specialFlatWithLimit);

        RentalPriceUtil.calculateRatesFromRentPeriod(
                LocalDate.of(2021, Month.AUGUST, 31).atTime(morning),
                LocalDate.of(2021, Month.SEPTEMBER, 4).atTime(evening), line);
        assertEquals(specialFlatWithLimit.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(specialFlatWithLimit.meterLimit.intValue(), line.meterLimit.intValue());
        assertEquals(1, specialFlatWithLimit.qty.intValue());
    }

    @Test
    public void testCalculateRates_SingleRate_withLength() {
        RentalPriceQtyDTO rate = specialMonthWithoutLimit;
        line.rentalPriceQtyDTOs = Arrays.asList(rate);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusDays(2 * rate.length), line);
        assertEquals(2, rate.qty.intValue());
        assertEquals(2 * rate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertNull(line.meterLimit);

    }

    @Test
    public void testCalculateRates_SingleRate_withLengthAndLimit() {
        RentalPriceQtyDTO rate = specialMonthWithLengthAndLimit;
        line.rentalPriceQtyDTOs = Arrays.asList(rate);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusDays(2 * rate.length), line);
        assertEquals(2 * rate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(2 * rate.meterLimit, line.meterLimit.intValue());
        assertEquals(2, rate.qty.intValue());
    }

    @Test
    public void testCalculateRates_DayRate_ShouldRoundUp() {
        line.rentalPriceQtyDTOs = Arrays.asList(dayRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(1), line);
        assertEquals(dayRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(dayRate.meterLimit.intValue(), line.meterLimit.intValue());
        assertEquals(1, dayRate.qty.intValue());

        RentalPriceUtil.calculateRatesFromRentPeriod(start, start.plusHours(25), line);
        assertEquals(2 * dayRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(2 * dayRate.meterLimit, line.meterLimit.intValue());
        assertEquals(2, dayRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_MonthlyRate_30days() {
        line.rentalPriceQtyDTOs = Arrays.asList(monthlyRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(
                LocalDate.of(2020, Month.AUGUST, 1).atTime(8,0),
                LocalDate.of(2020, Month.SEPTEMBER, 30).atTime(8,0), line);
        assertEquals(2, monthlyRate.qty.intValue());
        assertEquals(2 * monthlyRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(2 * monthlyRate.meterLimit, line.meterLimit.intValue());

        RentalPriceUtil.calculateRatesFromRentPeriod(
                LocalDate.of(2020, Month.AUGUST, 1).atTime(8,0),
                LocalDate.of(2020, Month.OCTOBER, 1).atTime(8,0), line);
        assertEquals(3, monthlyRate.qty.intValue());

        RentalPriceUtil.calculateRatesFromRentPeriod(
                LocalDate.of(2020, Month.FEBRUARY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.MARCH, 2).atStartOfDay(), line);
        assertEquals(1, monthlyRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_LessThanAWeek_SaturdayToWednesday_WeekendAndDays() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate, fiveDayRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(Sat.atStartOfDay(), NextMon.plusDays(2).atTime(9,0), line);
        assertEquals(1, weekendRate.qty.intValue());
        assertEquals(2, dayRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_LessThanAWeek_FridayAndWeekend_WeekendAndDays() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate, fiveDayRate);
        dayRate.startDayOfWeek = DayOfWeek.MONDAY;
        dayRate.endDayOfWeek = DayOfWeek.FRIDAY;

        RentalPriceUtil.calculateRatesFromRentPeriod(Fri.atTime(9,0), NextMon.atTime(9,0), line);
        assertEquals(1, dayRate.qty.intValue());
        assertEquals(1, weekendRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_LessThanAWeek_FridayToMonday_WeekendAndDays() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate, fiveDayRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(Fri.atTime(9,0), NextMon.atTime(17,0), line);

        //not optimal can't handle day before weekend
        assertEquals(4, dayRate.qty.intValue());
        //optimal
        //assertEquals(2, dayRate.qty.intValue());
        //assertEquals(1, weekendRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_LessThanAWeek_SixDays_Week() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate, fiveDayRate, weeklyRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(Fri.atTime(9,0), Fri.plusDays(6).atTime(17,0), line);
        assertEquals(1, weeklyRate.qty.intValue());
        assertNull(dayRate.qty);
        assertNull(weekendRate.qty);
        assertNull(fiveDayRate.qty);
    }

    @Test
    public void testCalculateRates_BigCase() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate, hourlyRate, monthlyRate, specialFlat, fiveDayRate,
                weeklyRate, specialSixMonths, specialTwoMonths, specialTwoWeeks);

        //long period, shouldn't take long to calculate
        RentalPriceUtil.calculateRatesFromRentPeriod(
                LocalDate.of(2021, Month.SEPTEMBER, 24).atTime(9,0),
                LocalDate.of(2022, Month.SEPTEMBER, 24).atTime(9,0).plusHours(2), line);
        assertEquals(2, specialSixMonths.qty.intValue());
        //assertEquals(1, fiveDayRate.qty.intValue());
//        assertEquals(2, hourlyRate.qty.intValue());
        assertNull(dayRate.qty);
        assertNull(weekendRate.qty);

    }

    @Test
    public void testCalculateRates_DaysAndWeekendsOnly_OneDay() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(Fri.atTime(9,0), Sat.atTime(9,0), line);
        assertEquals(1, dayRate.qty.intValue());
        assertNull(weekendRate.qty);

        RentalPriceUtil.calculateRatesFromRentPeriod(Sat.atTime(9,0), NextMon.atTime(9,0), line);
        assertEquals(1, weekendRate.qty.intValue());
        assertNull(dayRate.qty);

        RentalPriceUtil.calculateRatesFromRentPeriod(Sat.atTime(17,0), NextMon.atTime(9,0), line);
        assertEquals(1, weekendRate.qty.intValue());
        assertNull(dayRate.qty);
    }

    @Test
    public void testCalculateRates_DaysAndWeekends_WeekendIsMoreExpensive() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate);
        dayRate.startDayOfWeek = DayOfWeek.MONDAY;
        dayRate.endDayOfWeek = DayOfWeek.FRIDAY;
        dayRate.rate = BigDecimal.valueOf(200);
        weekendRate.rate = BigDecimal.valueOf(450);

        RentalPriceUtil.calculateRatesFromRentPeriod(Fri.atTime(9,0), NextMon.atTime(8,0), line);
        assertEquals(1, dayRate.qty.intValue());
        assertEquals(1, weekendRate.qty.intValue());
    }

    @Ignore //can't handle it properly
    public void testCalculateRates_DaysAndWeekendsOnly_MultipleWeekends() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(Fri.atTime(17,0), NextMon.plusDays(7).atTime(8,0), line);
        assertEquals(2, weekendRate.qty.intValue());
        assertEquals(6, dayRate.qty.intValue());

        RentalPriceUtil.calculateRatesFromRentPeriod(Sat.atTime(17,0), NextMon.plusDays(7).atTime(8,0), line);
        assertEquals(2, weekendRate.qty.intValue());
        assertEquals(5, dayRate.qty.intValue());

        RentalPriceUtil.calculateRatesFromRentPeriod(Sat.atTime(17,0), NextMon.plusDays(10).atTime(8,0), line);
        assertEquals(2, weekendRate.qty.intValue());
        assertEquals(8, dayRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_LessThanAWeek_SixDays_Week_RoundUpTheLastDay() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate, fiveDayRate, weeklyRate);

        RentalPriceUtil.calculateRatesFromRentPeriod( Fri.atTime(9,0), Fri.plusDays(6).atTime(17,0), line);
        assertEquals(1, weeklyRate.qty.intValue());
        assertNull(dayRate.qty);
        assertNull(weekendRate.qty);
        assertNull(fiveDayRate.qty);
    }

    @Test
    public void testCalculateRates_LessThanAWeek_MondayToWednesday_Days() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate, fiveDayRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(Mon.atTime(9,0), Wed.atTime(17,0), line);
        assertNull(weekendRate.qty);
        assertNull(fiveDayRate.qty);
        assertEquals(3, dayRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_LessThanAWeek_MondayToThursday_ButFiveDayWeekIsBest() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate, fiveDayRate);

        //start on weekday
        RentalPriceUtil.calculateRatesFromRentPeriod(Mon.atTime(9,0), Thu.atTime(17,0), line);
        assertNull(weekendRate.qty);
        assertNull(dayRate.qty);
        assertEquals(1, fiveDayRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_LessThanAWeek_TuesdayToFriday_ButFiveDayWeekIsBest()
    {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate, fiveDayRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(Tue.atStartOfDay(), Fri.atTime(17,0), line);

        assertNull(weekendRate.qty);
        assertNull(dayRate.qty);
        assertEquals(1, fiveDayRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_LessThanAWeek_MondayToFriday_FiveWeekDay() {
        line.rentalPriceQtyDTOs = Arrays.asList(weekendRate, dayRate, fiveDayRate);

        //5 weekday
        RentalPriceUtil.calculateRatesFromRentPeriod(Mon.atTime(9,0), Fri.atTime(17,0), line);
        assertNull(weekendRate.qty);
        assertNull(dayRate.qty);
        assertEquals(1, fiveDayRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_DayAndMonth_Single() {
        line.rentalPriceQtyDTOs = Arrays.asList(dayRate, monthlyRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(
                start, start.plusHours(1), line);

        assertEquals(1, dayRate.qty.intValue());
        assertNull(monthlyRate.qty);
        assertEquals(dayRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(dayRate.meterLimit.intValue(), line.meterLimit.intValue());


        RentalPriceUtil.calculateRatesFromRentPeriod(
                start, start.plusHours(25), line);
        assertEquals(2 * dayRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(2 * dayRate.meterLimit, line.meterLimit.intValue());
        assertEquals(2, dayRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_DayAndMonth_Multiple() {
        line.rentalPriceQtyDTOs = Arrays.asList(dayRate, monthlyRate);

        RentalPriceUtil.calculateRatesFromRentPeriod(
                LocalDate.of(2020, Month.AUGUST, 1).atStartOfDay(),
                LocalDate.of(2020, Month.SEPTEMBER, 30).atStartOfDay(), line);

        assertEquals(2, monthlyRate.qty.intValue());
        assertNull(dayRate.qty);
        assertEquals(2 * monthlyRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(2 * monthlyRate.meterLimit.intValue(), line.meterLimit.intValue());


        RentalPriceUtil.calculateRatesFromRentPeriod(
                LocalDate.of(2020, Month.AUGUST, 1).atStartOfDay(),
                LocalDate.of(2020, Month.OCTOBER, 1).atStartOfDay(), line);
        assertEquals(2, monthlyRate.qty.intValue());
        assertEquals(1, dayRate.qty.intValue());
        assertEquals(2*monthlyRate.rate.doubleValue() + dayRate.rate.doubleValue(), RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
        assertEquals(2*monthlyRate.meterLimit + dayRate.meterLimit, line.meterLimit.intValue());


        RentalPriceUtil.calculateRatesFromRentPeriod(
                LocalDate.of(2020, Month.AUGUST, 1).atStartOfDay(),
                LocalDate.of(2020, Month.NOVEMBER, 1).atStartOfDay(), line);
        assertEquals(3, monthlyRate.qty.intValue());
        assertEquals(2, dayRate.qty.intValue());
    }

    @Test
    public void testDaysBetween() {
        assertEquals(0, RentalPriceUtil.daysBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 1).atTime(0, 30),
                false)
        );

        assertEquals(1, RentalPriceUtil.daysBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 1).atTime(0, 30),
                true)
        );

        assertEquals(1, RentalPriceUtil.daysBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 2).atTime(10, 0),
                false)
        );

        assertEquals(2, RentalPriceUtil.daysBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 2).atTime(10, 0),
                true)
        );
    }


    @Test
    public void testWeeksBetween() {
        assertEquals(0, RentalPriceUtil.weeksBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 6).atTime(23, 30),
                false)
        );

        assertEquals(1, RentalPriceUtil.weeksBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 7).atTime(0, 30),
                false)
        );

        assertEquals(1, RentalPriceUtil.weeksBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 7).atTime(0, 30),
                true)
        );

        assertEquals(1, RentalPriceUtil.weeksBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 8).atTime(10, 0),
                false)
        );

        assertEquals(2, RentalPriceUtil.weeksBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 8).atTime(10, 0),
                true)
        );
    }

    @Test
    public void testMonthsBetween() {
        assertEquals("within a month", 0, RentalPriceUtil.monthsBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 31).atTime(23, 30),
                false)
        );

        assertEquals("within a month",1, RentalPriceUtil.monthsBetween(
                LocalDate.of(2020, Month.JULY, 1).atStartOfDay(),
                LocalDate.of(2020, Month.JULY, 10).atTime(23, 30),
                true)
        );

        assertEquals("spanning over two months", 0, RentalPriceUtil.monthsBetween(
                LocalDate.of(2020, Month.JULY, 21).atStartOfDay(),
                LocalDate.of(2020, Month.AUGUST, 19).atTime(23, 30),
                false)
        );

        assertEquals("spanning over two months",1, RentalPriceUtil.monthsBetween(
                LocalDate.of(2020, Month.JULY, 21).atStartOfDay(),
                LocalDate.of(2020, Month.AUGUST, 19).atTime(23, 30),
                true)
        );

        assertEquals(1, RentalPriceUtil.monthsBetween(
                LocalDate.of(2020, Month.JULY, 21).atStartOfDay(),
                LocalDate.of(2020, Month.AUGUST, 29).atTime(23, 30),
                false)
        );

        assertEquals(2, RentalPriceUtil.monthsBetween(
                LocalDate.of(2020, Month.JULY, 21).atStartOfDay(),
                LocalDate.of(2020, Month.AUGUST, 29).atTime(23, 30),
                true)
        );
    }

    @Test
    public void testApplyRate() {
        LocalDateTime monday = LocalDate.of(2021, Month.OCTOBER, 25).atTime(9,0);
        LocalDateTime saturday = LocalDate.of(2021, Month.OCTOBER, 23).atStartOfDay();
        LocalDateTime endDate =  LocalDate.of(2021, Month.NOVEMBER, 2).atStartOfDay();

        assertEquals(DayOfWeek.FRIDAY, RentalPriceUtil.apply(fiveDayRate, Mon.atTime(9,0), 1).getDayOfWeek());
        assertEquals(DayOfWeek.FRIDAY, RentalPriceUtil.apply(fiveDayRate, Mon.atStartOfDay().plusDays(1), 1).getDayOfWeek());
        assertEquals(DayOfWeek.FRIDAY, RentalPriceUtil.apply(fiveDayRate, Mon.atStartOfDay().plusDays(4), 1).getDayOfWeek());
        assertEquals(29, RentalPriceUtil.apply(fiveDayRate, monday.plusDays(4), 1).getDayOfMonth());

        assertEquals(DayOfWeek.MONDAY, RentalPriceUtil.apply(weekendRate, saturday, 1).getDayOfWeek());
        assertEquals(DayOfWeek.MONDAY, RentalPriceUtil.apply(weekendRate, saturday.plusDays(1), 1).getDayOfWeek());
        assertEquals(25, RentalPriceUtil.apply(weekendRate, saturday.plusDays(1), 1).getDayOfMonth());

        assertEquals("30 day, not a month", 24, RentalPriceUtil.apply(monthlyRate, monday, 1).getDayOfMonth());
        assertEquals(30, RentalPriceUtil.apply(weeklyRate, saturday, 1).getDayOfMonth());


        RentalPriceQtyDTO fourHourRate = new RentalPriceQtyDTO(RentalPriceDTO.TimeUnit.Hour, BigDecimal.valueOf(150), null, 4);
        fourHourRate.startTime = LocalTime.of(8, 0);
        fourHourRate.endTime = LocalTime.of(16, 0);
        LocalDateTime start = LocalDate.of(2021, Month.OCTOBER, 25).atTime(8, 0);
        LocalDateTime dateTime = RentalPriceUtil.apply(fourHourRate, start, 1);
        assertEquals(4, Duration.between(start, dateTime).toHours());

        dateTime = RentalPriceUtil.apply(weekendRate, Sat.atStartOfDay(), 1);
        assertEquals(weekendRate.endDayOfWeek, dateTime.getDayOfWeek());
        assertEquals(weekendRate.endTime, dateTime.toLocalTime());
    }

    @Test
    public void testSelection_DaysAndHours() {
        LocalDateTime monday = LocalDate.of(2021, Month.OCTOBER, 25).atStartOfDay();
        LocalDateTime saturday = LocalDate.of(2021, Month.OCTOBER, 23).atStartOfDay();

        List<RentalPriceQtyDTO> rates = Arrays.asList(dayRate, monthlyRate);
        RentalPriceUtil.Selection selection = RentalPriceUtil.Selection.createEmpty(rates);
        assertEquals(dayRate, selection.dayRate);
        assertNull(selection.weekendRate);
        boolean successful = selection.fillWithDaysAndHours(Sat.atTime(9,0), NextMon.atTime(9,0));
        assertTrue(successful);
        assertEquals(2, dayRate.qty.intValue());
        selection.updateWithBetterSelection();
        assertEquals(2, selection.qty[0], 0.01);
        assertEquals(0, selection.qty[1], 0.01);
        System.err.println(Arrays.toString(selection.qty));
    }

    @Ignore
    public void testSelection_Weekends() {
        RentalPriceUtil.Selection selection = RentalPriceUtil.Selection.createEmpty(Arrays.asList(dayRate, weekendRate, fiveDayRate));
        boolean successful = selection.fillWithDaysAndHours(Sat.atTime(9, 0), NextMon.atTime(9, 0));
        assertTrue(successful);
        assertEquals(1, weekendRate.qty.intValue());
        assertEquals(0, dayRate.qty.intValue());
        assertEquals(0, fiveDayRate.qty.intValue());
        selection.updateWithBetterSelection();
        System.err.println(Arrays.toString(selection.qty));

        selection = RentalPriceUtil.Selection.createEmpty(Arrays.asList(dayRate, weekendRate, fiveDayRate));
        successful = selection.fillWithDaysAndHours(Sat.atTime(9, 0), NextMon.atTime(17, 0));
        assertTrue(successful);
        selection.updateWithBetterSelection();
        System.err.println(Arrays.toString(selection.qty));
        assertEquals(1, weekendRate.qty.intValue());
        assertEquals(1, dayRate.qty.intValue());
        assertEquals(0, fiveDayRate.qty.intValue());

        selection = RentalPriceUtil.Selection.createEmpty(Arrays.asList(dayRate, weekendRate, fiveDayRate));
        successful = selection.fillWithDaysAndHours(Sat.atStartOfDay(), NextMon.plusDays(4).atTime(17,0));
        assertTrue(successful);
        selection.updateWithBetterSelection();
        System.err.println(Arrays.toString(selection.qty));
        assertEquals(1, weekendRate.qty.intValue());
        assertEquals(0, dayRate.qty.intValue());
        assertEquals(1, fiveDayRate.qty.intValue());

        selection = RentalPriceUtil.Selection.createEmpty(Arrays.asList(dayRate, weekendRate, fiveDayRate));
        successful = selection.fillWithDaysAndHours(Fri.atTime(9,0), NextMon.plusDays(4).atTime(17,0));
        assertTrue(successful);
        selection.updateWithBetterSelection();
        System.err.println(Arrays.toString(selection.qty));
        assertEquals(1, weekendRate.qty.intValue());
        assertEquals(1, dayRate.qty.intValue());
        assertEquals(1, fiveDayRate.qty.intValue());

        selection = RentalPriceUtil.Selection.createEmpty(Arrays.asList(dayRate, weekendRate, fiveDayRate));
        successful = selection.fillWithDaysAndHours(Fri.atTime(9,0), NextMon.plusDays(4).atTime(17,0));
        assertTrue(successful);
        selection.updateWithBetterSelection();
        System.err.println(Arrays.toString(selection.qty));
        assertEquals(1, weekendRate.qty.intValue());
        assertEquals(1, dayRate.qty.intValue());
        assertEquals(1, fiveDayRate.qty.intValue());

        selection = RentalPriceUtil.Selection.createEmpty(Arrays.asList(weekendRate, fiveDayRate));
        successful = selection.fillWithDaysAndHours(Mon.atTime(9,0), NextMon.atTime(17,0).plusDays(4));
        assertTrue(successful);
        selection.updateWithBetterSelection();
        System.err.println(Arrays.toString(selection.qty));
        assertEquals(1, weekendRate.qty.intValue());
        assertEquals(2, fiveDayRate.qty.intValue());

        selection = RentalPriceUtil.Selection.createEmpty(Arrays.asList(dayRate, fiveDayRate));
        successful = selection.fillWithDaysAndHours(Fri.atTime(9,0), NextMon.atTime(17,0).plusDays(4));
        assertTrue(successful);
        selection.updateWithBetterSelection();
        System.err.println(Arrays.toString(selection.qty));
        assertEquals(3, dayRate.qty.intValue());
        assertEquals(1, fiveDayRate.qty.intValue());
    }

    @Test
    public void testCalculateRates_periodWithSeconds() {
        RentalPriceQtyDTO dayRate = new RentalPriceQtyDTO(RentalPriceConst.DAY, BigDecimal.valueOf(200), 8, 1);
        RentalPriceQtyDTO weeklyRate = new RentalPriceQtyDTO(RentalPriceConst.WEEK, BigDecimal.valueOf(750), 40, 7);
        RentalPriceQtyDTO monthlyRate = new RentalPriceQtyDTO(RentalPriceConst.MONTH, BigDecimal.valueOf(2000), 160, 30);
        RentalPriceQtyDTO weekendRate = new RentalPriceQtyDTO(RentalPriceConst.WEEKEND, BigDecimal.valueOf(300), 12, 2);
        line.rentalPriceQtyDTOs = Arrays.asList(dayRate, weekendRate, monthlyRate, weeklyRate);

        LocalDateTime start = LocalDateTime.of(2022, Month.APRIL, 13, 16, 43, 7);
        LocalDateTime end = LocalDateTime.of(2022, Month.APRIL, 14, 16, 43, 9);

        RentalPriceUtil.calculateRatesFromRentPeriod(start, end, line);
        assertEquals(200, RentalPriceUtil.getTotalPrice(line.rentalPriceQtyDTOs), 0.01);
    }
}
