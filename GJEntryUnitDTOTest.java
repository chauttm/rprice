package com.gem.dto;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

public class GJEntryUnitDTOTest {
    UUID branchId_1 = UUID.randomUUID();
    UUID branchId_2 = UUID.randomUUID();
    UUID branchId_3 = UUID.randomUUID();
    String BANK_ACCOUNT = "BANK";
    String SALES = "SALES";
    String INTER_STORE = "IS";
    String COS_PART = "COS";

    @Test
    public void testReverse() {
        GJEntryUnitDTO cash = createEntry("D", 100, BANK_ACCOUNT, branchId_1);
        cash.id = UUID.randomUUID();
        GJEntryUnitDTO reverser = GJEntryUnitDTO.doReverse(cash);
        assertTrue(cash.reversed);
        assertTrue(reverser.reversed);
        assertNull(reverser.id);
        assertFalse(reverser.isDebit());
        assertEquals(cash.id, reverser.reversedId);
        assertEquals(cash.amount.doubleValue(), reverser.amount.doubleValue(), 0.01);
        assertEquals(cash.branchId, reverser.branchId);
        assertEquals(cash.accountCode, reverser.accountCode);

        //cannot reverse the same line again
        try {
            GJEntryUnitDTO.doReverse(cash);
            fail();
        }
        catch (Exception e) {

        }

    }

    @Test
    public void testCreateInterstores_Empty() {
        List<GJEntryUnitDTO> newLines = new ArrayList<>();
        List<GJEntryUnitDTO> interstores = GJEntryUnitDTO.createInterstores(newLines, INTER_STORE);
        assertTrue(interstores.isEmpty());
    }

    @Test
    public void testCreateInterstores_OneStore_DoNothing() {
        List<GJEntryUnitDTO> newLines = new ArrayList<>();
        newLines.add(createEntry("D", 100, BANK_ACCOUNT, branchId_1));
        newLines.add(createEntry("C", 100, SALES, branchId_1));
        List<GJEntryUnitDTO> interstores = GJEntryUnitDTO.createInterstores(newLines, INTER_STORE);
        assertTrue(interstores.isEmpty());
    }
    
    @Test
    public void testCreateInterstores_TwoStores() {
        List<GJEntryUnitDTO> newLines = new ArrayList<>();
        newLines.add(createEntry("D", 100, BANK_ACCOUNT, branchId_1));
        newLines.add(createEntry("C", 100, SALES, branchId_2));
        List<GJEntryUnitDTO> interstores = GJEntryUnitDTO.createInterstores(newLines, INTER_STORE);
        assertEquals(2, interstores.size());
        GJEntryUnitDTO first = interstores.get(0);
        GJEntryUnitDTO second = interstores.get(1);
        assertEquals(100, first.amount.doubleValue(), 0.01);
        assertEquals(100, second.amount.doubleValue(), 0.01);

        assertEquals(INTER_STORE, first.accountCode);
        assertEquals(INTER_STORE, second.accountCode);

        if (first.isDebit()) {
            assertEquals(branchId_2, first.branchId);
            assertFalse(second.isDebit());
            assertEquals(branchId_1, second.branchId);
        }
        else {
            assertEquals(branchId_2, second.branchId);
            assertFalse(first.isDebit());
            assertEquals(branchId_1, first.branchId);
        }
    }

    @Test
    public void testCreateInterstores_ThreeStores() {
        List<GJEntryUnitDTO> newLines = new ArrayList<>();
        newLines.add(createEntry("D", 100, BANK_ACCOUNT, branchId_1));
        newLines.add(createEntry("C", 110, SALES, branchId_2));
        newLines.add(createEntry("D", 10, COS_PART, branchId_2));

        newLines.add(createEntry("D", 100, BANK_ACCOUNT, branchId_3));
        newLines.add(createEntry("C", 100, SALES, branchId_3));

        List<GJEntryUnitDTO> interstores = GJEntryUnitDTO.createInterstores(newLines, INTER_STORE);
        assertEquals(2, interstores.size());
        GJEntryUnitDTO first = interstores.get(0);
        GJEntryUnitDTO second = interstores.get(1);
        assertEquals(100, first.amount.doubleValue(), 0.01);
        assertEquals(100, second.amount.doubleValue(), 0.01);

        assertEquals(INTER_STORE, first.accountCode);
        assertEquals(INTER_STORE, second.accountCode);

        if (first.isDebit()) {
            assertEquals(branchId_2, first.branchId);
            assertFalse(second.isDebit());
            assertEquals(branchId_1, second.branchId);
        }
        else {
            assertEquals(branchId_2, second.branchId);
            assertFalse(first.isDebit());
            assertEquals(branchId_1, first.branchId);
        }
    }

    public static GJEntryUnitDTO createEntry(String debitCredit, double amount, String accountCode, UUID branchId) {
        GJEntryUnitDTO dto = new GJEntryUnitDTO();
        dto.accountCode = accountCode;
        dto.amount = BigDecimal.valueOf(amount);
        dto.branchId = branchId;
        dto.debitCredit = debitCredit;
        dto.lineId = UUID.randomUUID();
        return dto;
    }
}
