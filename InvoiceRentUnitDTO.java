package com.gem.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gem.dto.util.RentalPriceUtil;
import com.gem.model.common.CheckListLog;
import com.gem.model.common.InvoiceRentUnit;
import com.gem.utils.MathUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class InvoiceRentUnitDTO extends InvoiceLineDTO {
    public UUID equipmentId;
    public LocalDateTime startDate;
    public LocalDateTime endDate;
    public LocalDateTime deliveryDate;
    public LocalDateTime returnDate;
    public boolean isDiscountByPercentage;
    public UUID mainAttachmentLineId;
    public UUID deliverCheckListLogId;
    public UUID returnCheckListLogId;
    public boolean checkOutDone;
    public boolean checkInDone;
    public UUID deliverLogId;
    public UUID returnLogId;
    public boolean customerPickup = true;
    public boolean customerReturn = true;
    public List<RentalPriceQtyDTO> rentalPriceQtyDTOs;
    public Integer meterLimit;
    public Double realMeter;
    public BigDecimal totalContracted = BigDecimal.ZERO;
    public BigDecimal discountContracted = BigDecimal.ZERO;
    public Double percentDiscountContracted;
    public Double overTime;
    public BigDecimal discountOvercharge = BigDecimal.ZERO;
    public BigDecimal totalOvercharge = BigDecimal.ZERO;
    public Double percentDiscountOvercharge;
    public BigDecimal overchargeRate;

    public InvoiceRentUnitDTO() {

    }

    public InvoiceRentUnitDTO(InvoiceRentUnit invoiceRentUnit) {
        this(invoiceRentUnit, invoiceRentUnit.getDeliverCheckListLog(), invoiceRentUnit.getReturnCheckListLog());
    }

    public InvoiceRentUnitDTO(InvoiceRentUnit invoiceRentUnit, CheckListLog deliverCheckListLog, CheckListLog returnCheckListLog)
    {
        super(invoiceRentUnit);
        this.equipmentId = invoiceRentUnit.getEquipment().getWebId();
        this.startDate = invoiceRentUnit.getStartDate();
        this.endDate = invoiceRentUnit.getEndDate();
        this.deliveryDate = invoiceRentUnit.getDeliveryDate();
        this.returnDate = invoiceRentUnit.getReturnDate();
        this.isDiscountByPercentage = invoiceRentUnit.isDiscountByPercentage();
        if (deliverCheckListLog != null)
        {
            this.deliverCheckListLogId = deliverCheckListLog.getWebId();
            this.checkOutDone = deliverCheckListLog.isDone();
        }
        if (returnCheckListLog != null)
        {
            this.returnCheckListLogId = returnCheckListLog.getWebId();
            this.checkInDone = returnCheckListLog.isDone();
            this.realMeter = (deliverCheckListLog == null || deliverCheckListLog.getMeter() == null || returnCheckListLog.getMeter() == null) ? null
                    : (MathUtils.rounding(returnCheckListLog.getMeter() - deliverCheckListLog.getMeter()));
        }
        this.mainAttachmentLineId = invoiceRentUnit.getMainAttachmentLineId();
        this.deliverLogId = invoiceRentUnit.getDeliverLogId();
        this.returnLogId = invoiceRentUnit.getReturnLogId();
        customerPickup = invoiceRentUnit.isCustomerPickUp();
        customerReturn = invoiceRentUnit.isCustomerReturn();
        this.totalContracted = invoiceRentUnit.getTotalContracted();
        this.discountContracted = invoiceRentUnit.getDiscountContracted();
        this.overTime = invoiceRentUnit.getOverTime();
        this.discountOvercharge = invoiceRentUnit.getDiscountOvercharge();
        this.totalOvercharge = invoiceRentUnit.getTotalOvercharge();

    }

    public void onUpdateTotalContracted(int scale) {
        BigDecimal beforeDiscount = BigDecimal.valueOf(RentalPriceUtil.getTotalPrice(rentalPriceQtyDTOs)).setScale(scale, RoundingMode.HALF_UP);
        discountContracted = beforeDiscount.subtract(totalContracted);

        updateGeneralPricing(scale);
        percentDiscountContracted = discountContracted.divide(beforeDiscount, 4, RoundingMode.HALF_UP).doubleValue() * 100;
    }

    public void onUpdateTotalOvercharge(int scale) {
        BigDecimal beforeDiscount = overchargeRate.multiply(BigDecimal.valueOf(overTime)).setScale(scale, RoundingMode.HALF_UP);
        discountOvercharge = beforeDiscount.subtract(totalOvercharge);

        updateGeneralPricing(scale);
        percentDiscountOvercharge = discount.divide(beforeDiscount, 4, RoundingMode.HALF_UP).doubleValue() * 100;
    }

    public void onUpdateDiscountContracted(int scale) {
        BigDecimal beforeDiscount = BigDecimal.valueOf(RentalPriceUtil.getTotalPrice(rentalPriceQtyDTOs)).setScale(scale, RoundingMode.HALF_UP);
        totalContracted = beforeDiscount.subtract(discountContracted);

        updateGeneralPricing(scale);
        percentDiscount = null;
    }

    public void onUpdateDiscountOvercharge(int scale) {
        BigDecimal beforeDiscount = overchargeRate.multiply(BigDecimal.valueOf(overTime)).setScale(scale, RoundingMode.HALF_UP);
        discountOvercharge = beforeDiscount.subtract(discountOvercharge);

        updateGeneralPricing(scale);
        percentDiscount = null;
    }

    public void onUpdateDiscountPercentContracted(int scale) {
        BigDecimal beforeDiscount = BigDecimal.valueOf(RentalPriceUtil.getTotalPrice(rentalPriceQtyDTOs)).setScale(scale, RoundingMode.HALF_UP);
        discountContracted = beforeDiscount.multiply(BigDecimal.valueOf(percentDiscount / 100)).setScale(scale, RoundingMode.HALF_UP);
        totalContracted = beforeDiscount.subtract(discountContracted);

        updateGeneralPricing(scale);
    }

    public void onUpdateDiscountPercentOvercharge(int scale) {
        BigDecimal beforeDiscount = overchargeRate.multiply(BigDecimal.valueOf(overTime)).setScale(scale, RoundingMode.HALF_UP);
        discountOvercharge = beforeDiscount.multiply(BigDecimal.valueOf(percentDiscount / 100)).setScale(scale, RoundingMode.HALF_UP);
        totalOvercharge = beforeDiscount.subtract(discountOvercharge);

        updateGeneralPricing(scale);
    }

    public void onUpdateRates(int scale) {
        BigDecimal beforeDiscount = BigDecimal.valueOf(RentalPriceUtil.getTotalPrice(rentalPriceQtyDTOs)).setScale(scale, RoundingMode.HALF_UP);
        if (percentDiscountContracted != null ) {
            discountContracted = beforeDiscount.multiply(BigDecimal.valueOf(percentDiscount / 100)).setScale(scale, RoundingMode.HALF_UP);
        }
        totalContracted = beforeDiscount.subtract(discountContracted);

        if (realMeter != null) {
            overTime = realMeter - meterLimit;
            BigDecimal overchargeBeforeDiscount = overchargeRate.multiply(BigDecimal.valueOf(overTime)).setScale(scale, RoundingMode.HALF_UP);
            if (percentDiscountOvercharge != null) {
                discountOvercharge = overchargeBeforeDiscount.multiply(BigDecimal.valueOf(percentDiscount / 100)).setScale(scale, RoundingMode.HALF_UP);
            }
            totalOvercharge = overchargeBeforeDiscount.subtract(discountOvercharge);
        }

        updateGeneralPricing(scale);
    }

    public void onUpdateOvertime(int scale) {
        BigDecimal overchargeBeforeDiscount = overchargeRate.multiply(BigDecimal.valueOf(overTime)).setScale(scale, RoundingMode.HALF_UP);
        if (percentDiscountOvercharge != null) {
            discountOvercharge = overchargeBeforeDiscount.multiply(BigDecimal.valueOf(percentDiscount / 100)).setScale(scale, RoundingMode.HALF_UP);
        }
        totalOvercharge = overchargeBeforeDiscount.subtract(discountOvercharge);

        updateGeneralPricing(scale);
    }

    public void updateGeneralPricing(int scale) {
        extPrice = totalContracted.add(totalOvercharge);
        discount = discountContracted.add(discountOvercharge);
        // updateDistributedDiscountAfterExtPriceChange();  //Android and iOS need to implement this functions
        calculateTaxAndPreTaxPrice(scale);
    }

    public void onUpdateTaxPercent(int scale) {
        calculateTaxAndPreTaxPrice(scale);
    }

    private void calculateTaxAndPreTaxPrice(int scale) {
        BigDecimal priceAfterAllDiscounted = extPrice.subtract(distributedDiscount);
        if (percentTax == null || percentTax <= 0) {
            tax = BigDecimal.ZERO;
            preTaxPrice = priceAfterAllDiscounted;
        } else if (includeVat) {
            BigDecimal taxOverPrice = BigDecimal.valueOf(percentTax / (percentTax + 100));
            tax = priceAfterAllDiscounted.multiply(taxOverPrice).setScale(scale, RoundingMode.HALF_UP);
            preTaxPrice = priceAfterAllDiscounted.subtract(tax);
        } else {
            BigDecimal taxOverPrice = BigDecimal.valueOf(percentTax / 100.0);
            tax = priceAfterAllDiscounted.multiply(taxOverPrice).setScale(scale, RoundingMode.HALF_UP);
            preTaxPrice = priceAfterAllDiscounted;
        }
    }

    @JsonIgnore
    public boolean isReturned()
    {
        return !deleted && returnDate != null;
    }

    @JsonIgnore
    public boolean isRented()
    {
        return !deleted && deliveryDate != null;
    }

    @JsonIgnore
    public LocalDateTime getActualDeliveryDate()
    {
        return deliveryDate != null ? deliveryDate : startDate;
    }

    @JsonIgnore
    public LocalDateTime getActualReturnDate()
    {
        return returnDate != null ? returnDate : endDate;
    }
}
