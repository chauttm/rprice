package com.gem.dto.util;

import com.gem.dto.InvoiceRentUnitDTO;
import com.gem.dto.RentalPriceDTO;
import com.gem.dto.RentalPriceQtyDTO;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static com.gem.dto.constant.RentalPriceConst.*;

public class RentalPriceUtil {
    public static long hoursBetween(LocalDateTime date, LocalDateTime endDate) {
        return Math.round(Math.ceil(Duration.between(date, endDate).toMinutes() / 60.0));
    }

    public static long daysBetween(LocalDateTime date, LocalDateTime endDate, boolean roundingUp) {
        long hours = hoursBetween(date, endDate);
        return roundingUp ? Math.round(Math.ceil(hours/24.0)) : hours/24;
    }

    public static class Selection {
        double total = Double.MAX_VALUE;
        public double [] qty;

        public RentalPriceQtyDTO dayRate;
        public RentalPriceQtyDTO fiveDateRate;
        public RentalPriceQtyDTO weekendRate;
        public RentalPriceQtyDTO hourRate;
        List<RentalPriceQtyDTO> list;
        List<RentalPriceQtyDTO> nonDayRates  = new ArrayList<>();

        private Selection() {}

        public static Selection createEmpty(List<RentalPriceQtyDTO> list) {
            Selection selection = new Selection();
            selection.qty = new double[list.size()];
            Arrays.fill(selection.qty, 0);

            selection.list = list;
            for (RentalPriceQtyDTO rate : list) {
                switch (rate.getType()) {
                    case DAY: selection.dayRate = rate; break;
                    case HOUR: selection.hourRate = rate; break;
                    default: if (rate.hasTimeConstraints()) selection.nonDayRates.add(rate);
                }
            }
            return selection;
        }

        public void updateWithBetterSelection() {
            double total = getTotalPrice(list);
            if (total > this.total) return;
            this.total = total;
            for (int i = 0; i < list.size(); i++)
                qty[i] = list.get(i).qty == null ? 0 : list.get(i).qty;
        }

        public void populate(List<RentalPriceQtyDTO> list) {
            for (int i = 0; i < list.size(); i++)
                list.get(i).qty = Math.abs(qty[i]) < 0.5 ? null : qty[i];
        }

        public boolean fillWithDaysAndHours(LocalDateTime startDate, LocalDateTime endDate) {
            if (dayRate != null) dayRate.qty = 0.0;
            if (hourRate != null) hourRate.qty = 0.0;
            while (startDate.isBefore(endDate)) {
                RentalPriceQtyDTO bestRate = hourRate;
                long qty = (hourRate != null) ? hoursBetween(startDate, endDate) : 0;
                double minCost = (hourRate != null) ? hourRate.rate.doubleValue() * qty : Double.MAX_VALUE;
                if (dayRate != null && dayRate.rate.doubleValue() < minCost) {
                    bestRate = dayRate;
                    qty = 1;
                }
                if (bestRate != null) startDate = apply(bestRate, startDate, qty);
                else return false;
            }
            return true;
        }
    }

    public static void calculateRatesFromRentPeriod(LocalDateTime startDate, LocalDateTime endDate, InvoiceRentUnitDTO line)
    {
        if (startDate.plusMinutes(1).isAfter(endDate)) return;
        startDate = startDate.truncatedTo(ChronoUnit.MINUTES);
        endDate = endDate.truncatedTo(ChronoUnit.MINUTES);

        List<RentalPriceQtyDTO> list = line.rentalPriceQtyDTOs;
        if (list == null || list.isEmpty()) return;
        list.forEach(rate->rate.qty = rate.hasTimeConstraints() ? 0.0 : null);
        list.sort(new SortByLengthDesc());
        if (list.get(0).hasTimeConstraints()) {
            Selection best = Selection.createEmpty(list);
            findBestSelection(startDate, endDate, 0, best);
            best.populate(list);
        }
        else {
            RentalPriceQtyDTO flat = list.stream().filter(rate -> !rate.hasTimeConstraints()).findAny().orElse(null);
            if (flat != null) flat.qty = 1.0;
            else list.get(0).qty = 1.0;
        }
        line.meterLimit = getTotalLimit(list);
    }

    private static void findBestSelection(LocalDateTime startDate, LocalDateTime endDate,
                                          int current, Selection best)
    {
        if (current >= best.nonDayRates.size() || !startDate.isBefore(endDate)) {
            if (best.fillWithDaysAndHours(startDate, endDate)) best.updateWithBetterSelection();
            return;
        }

        RentalPriceQtyDTO rate = best.nonDayRates.get(current);
        long qty = selectQty(startDate, endDate, rate);
        findBestSelection(apply(rate, startDate, qty), endDate,current + 1, best);

        if (qty > 1) {
            rate.qty = 0.0;
            findBestSelection(apply(rate, startDate, qty-1), endDate, current + 1, best);
        }

        if (qty > 0) {
            rate.qty = 0.0;
            findBestSelection(startDate, endDate, current + 1, best);
        }
    }

    public static LocalDateTime apply(RentalPriceQtyDTO rate, LocalDateTime startDate, long times) {
        return times > 0 ? rate.apply(startDate, times) : startDate;
    }

    public static double getTotalPrice(List<RentalPriceQtyDTO> list) {
        double total = 0;
        for (RentalPriceQtyDTO rentalPriceQtyDTO : list) total += rentalPriceQtyDTO.getPrice();
        return total;
    }

    public static long selectQty(LocalDateTime startDate, LocalDateTime endDate, RentalPriceQtyDTO rate) {
        if (rate.hasStartAndEnd()) return  (fitStartAndEnd(rate, startDate, endDate)) ? 1 : 0;
        else if (rate.timeUnit != null && rate.length != null) {
            switch (rate.timeUnit) {
                    case Day: return (long)Math.ceil(1.0 * daysBetween(startDate, endDate, true) / rate.length);
                    case Hour: return (long)Math.ceil(1.0 * hoursBetween(startDate, endDate) / rate.length);
                    case Minute: return (long)Math.ceil(1.0 * Duration.between(startDate, endDate).toMinutes() / rate.length);
                    default: throw new RuntimeException("Unsupport time unit " + rate.timeUnit);
            }
        }
        throw new RuntimeException("missing timeUnit or length");
    }

    public static boolean fitStartAndEnd(RentalPriceQtyDTO rate, LocalDateTime startDate, LocalDateTime endDate)
    {
        if ((rate.startDayOfWeek == null || rate.startDayOfWeek.ordinal() == startDate.getDayOfWeek().ordinal())
                && rate.startTime != null && rate.startTime.isAfter(startDate.toLocalTime()))
                return false;  // start too early

        if ((rate.endDayOfWeek == null || rate.endDayOfWeek.ordinal() == startDate.getDayOfWeek().ordinal())
                && rate.endTime != null && !rate.endTime.isAfter(startDate.toLocalTime()))
            return false;  // start too late

        if (rate.startDayOfWeek != null && rate.endDayOfWeek != null) {
            int minStart = rate.startDayOfWeek.getValue();
            int start = (startDate.getDayOfWeek().getValue() + 7 - minStart) % 7;
            int maxEnd = (rate.endDayOfWeek.getValue() + 7 - minStart) % 7;
            return start>=0 && maxEnd >= start;
        }
        return true;
    }

    public static class SortByLengthDesc implements Comparator<RentalPriceDTO>
    {
        public int compare(RentalPriceDTO a, RentalPriceDTO b)
        {
            Double aLength = a.getLengthInDays();
            Double bLength = b.getLengthInDays();
            if (aLength == null && bLength == null) return 0;
            if (aLength == null) return 1;
            if (bLength == null) return -1;
            return Double.compare(bLength, aLength);
        }
    }

    public static Double getTotalLimit(List<RentalPriceQtyDTO> list) {
        double total = 0;
        for (RentalPriceQtyDTO rate : list) {
            if (rate.meterLimit == null && rate.qty != null) return null;
            if (rate.qty != null) total = (int)Math.round(total + rate.meterLimit * rate.qty);
        }
        return total;
    }

    public static long weeksBetween(LocalDateTime date, LocalDateTime endDate, boolean roundingUp) {
        long daysBetween = daysBetween(date, endDate, true);
        return roundingUp ? Math.round(Math.ceil(daysBetween / 7.0)) : daysBetween / 7;
    }

    public static long monthsBetween(LocalDateTime startDate, LocalDateTime endDate, boolean roundingUp) {
        if (startDate.isAfter(endDate)) return 0;
        long years = endDate.getYear() - startDate.getYear();
        long months = endDate.getMonthValue() - startDate.getMonthValue();
        long monthDiff = years * 12 + months;

        if (startDate.plusMonths(monthDiff).isEqual(endDate)) return monthDiff;

        if (roundingUp) {
            return (startDate.plusMonths(monthDiff).isAfter(endDate)) ? monthDiff : monthDiff + 1;
        }
        return startDate.plusMonths(monthDiff).isBefore(endDate) ? monthDiff : monthDiff - 1;
    }

}
