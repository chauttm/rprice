package com.gem.dto;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDateTime;

import static com.gem.dto.constant.RentalPriceConst.*;
import static com.gem.dto.constant.RentalPriceConst.HOUR;

public class RentalPriceQtyDTO extends RentalPriceDTO {
    public Double qty;

    public RentalPriceQtyDTO() {

    }

    public RentalPriceQtyDTO(String title, BigDecimal rate, Integer meterLimit, Integer length) {
        super(title, rate, meterLimit, length);
    }

    public RentalPriceQtyDTO(String title, BigDecimal rate, Integer meterLimit, Integer length, Double qty) {
        super(title, rate, meterLimit, length);
        this.qty = qty;
    }

    public RentalPriceQtyDTO(TimeUnit timeUnit, BigDecimal rate, Integer meterLimit, int length) {
        super(timeUnit, rate, meterLimit, length);
    }

    public RentalPriceQtyDTO(DayOfWeek from, DayOfWeek to, BigDecimal rate, Integer meterLimit) {
        super(from, to, rate, meterLimit);
    }

    public double getPrice() {
        return (qty == null) ? 0.0 : qty * rate.doubleValue();
    }

    public LocalDateTime apply(LocalDateTime startDate, long times) {
        qty = qty == null ? times : qty + times;

        if (length != null) {
            switch (timeUnit) {
                case Day: return startDate.plusDays(length * times);
                case Hour: return startDate.plusHours(length * times);
                case Minute: return startDate.plusMinutes(length * times);
                default: throw new RuntimeException("Unsupport time unit " + timeUnit);
            }
        }
        LocalDateTime date = startDate;
        if (endTime != null) date = date.toLocalDate().atTime(endTime);
        if (endDayOfWeek != null) {
            while (date.getDayOfWeek().ordinal() != endDayOfWeek.ordinal()) date = date.plusDays(1);
        }
        return date;
    }
}
