package com.gem.dto;

import com.gem.dto.constant.MessageKey;
import com.gem.exception.MoolaServiceException;
import com.gem.model.common.Branch;
import com.gem.model.common.Loan;
import com.gem.model.common.Payable;
import com.gem.model.common.Receipt;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by qsoft on 7/19/17.
 */
public class GJEntryUnitDTO extends ClassNameDTO
{
    public UUID id;  //not null means already posted
    public String accountCode;
    public UUID accountId;
    public String debitCredit;  // valid value: C c D d, other will get exceptions
    public BigDecimal amount;   //user entered value
    public BigDecimal amountInOriginalCurrency;
    public UUID branchId;
    public UUID whoId;  //payroll's staffId, receivable's customerId, payable's supplierId, inventory's unitId
    public LocalDate dueDate; // for debit to receivables and credit to payables
    public UUID lineId; // payableId, loanId, receiptId

    public UUID staffId;
    public LocalDateTime dateCreated;
    public boolean reversed = false;
    public UUID reversedId;
    public String description;

    public UUID inventoryItemId; // equipmentId, partId

    public GJEntryUnitDTO()
    {

    }

    public GJEntryUnitDTO(String accountCode, String debitCredit, BigDecimal amount)
    {
        this.accountCode = accountCode;
        this.debitCredit = debitCredit;
        this.amount = amount;
    }

    public GJEntryUnitDTO(String accountCode, boolean debit, BigDecimal amount, Branch branch)
    {
        this.accountCode = accountCode;
        setDebit(debit);
        this.amount = amount;
        this.branchId = branch.getWebId();
    }

    public GJEntryUnitDTO(Loan loan) {
        setDebit(true);
        amount = loan.getAmount();
        branchId = loan.getBranch().getWebId();
        dueDate = loan.getDueDate();
        lineId = loan.getWebId();
        whoId = loan.getCustomer().getWebId();
    }

    public GJEntryUnitDTO(Payable payable) {
        setDebit(payable.getAmount().compareTo(BigDecimal.ZERO) < 0);
        amount = payable.getAmount().abs();
        branchId = payable.getBranch().getWebId();
        dueDate = payable.getDueDate();
        lineId = payable.getWebId();
        whoId = payable.getSupplier().getWebId();
    }

    public GJEntryUnitDTO(Receipt receipt) {
        setDebit(false);
        amount = receipt.getTotal();
        branchId = receipt.getBranch().getWebId();
        lineId = receipt.getWebId();
        whoId = receipt.getCustomer().getWebId();
    }

    public boolean isDebit() {
        switch (debitCredit.charAt(0)) {
            case 'd':
            case 'D': return true;
            case 'c':
            case 'C': return false;
            default: throw new RuntimeException("Invalid debitCredit " + debitCredit);
        }
    }

    public void setDebit(boolean debit) {
        debitCredit = debit ? "D" : "C";
    }



    /*
    * data structures:
    *   List<GJEntryUnitDTO> posted: list of posted TU's sent by MT
    *   List<GJEntryUnitDTO> newList: list of newly added and not-yet-posted TU's
    * what to show in the upper section:  posted (reversed and interstore hidden), new list
    * what to show in the lower section:  posted, new list, and new interstores
    *
    * Event changeDate:
    *     1.changeDate(posted, newList)
    *     2. update lower section with posted, newList, and createInterstore(newList, interstoreCode)
    *
    * Event reverse:
    *     1. newList.add(doReverse(reversedLine))     reversedLine should be an item in posted list
    *     2. update lower section with posted, newList, and createInterstore(newList, interstoreCode)
    *
    * Event Done:
    *     postGJE (newlist)
    * */

    public static void changeDate(List<GJEntryUnitDTO> posted, List<GJEntryUnitDTO> newList) {
        posted.stream().filter(it -> !it.reversed).forEach(it -> {
            newList.add(doReverse(it));
            try {
                GJEntryUnitDTO newLine = (GJEntryUnitDTO) it.clone();
                newLine.id = null;
                newList.add(newLine);
            } catch (CloneNotSupportedException e) {}
        });
    }

    public static GJEntryUnitDTO doReverse(GJEntryUnitDTO reversed) {
        reversed.reversed = true;
        GJEntryUnitDTO reverser = new GJEntryUnitDTO();
        reverser.reversed = true;
        reverser.reversedId = reversed.id;
        reverser.accountCode = reversed.accountCode;
        reverser.setDebit(!reversed.isDebit());
        reverser.branchId = reversed.branchId;
        reverser.amount = reversed.amount;
        return reverser;
    }

    public static GJEntryUnitDTO doReverse(UUID reversedId, List<GJEntryUnitDTO> posted) {
        GJEntryUnitDTO reversed = posted.stream()
                .filter(it -> it.id.equals(reversedId)).findAny().orElse(null);
        if (reversed == null || reversed.reversed)
            throw new MoolaServiceException(MessageKey.DATA_OUT_OF_DATE);
        return doReverse(reversed);
    }

    public static List<GJEntryUnitDTO> createInterstores(List<GJEntryUnitDTO> entryUnits, String InterstoreCode) {
        Map<UUID, BigDecimal> debitByBranch = new HashMap<>();
        Map<UUID, BigDecimal> creditByBranch = new HashMap<>();
        entryUnits.forEach(entryUnit -> {
            debitByBranch.put(entryUnit.branchId, BigDecimal.ZERO);
            creditByBranch.put(entryUnit.branchId, BigDecimal.ZERO);
        });
        entryUnits.forEach(entryUnit -> {
            if (entryUnit.isDebit()) {
                debitByBranch.put(entryUnit.branchId, entryUnit.amount.add(debitByBranch.get(entryUnit.branchId)));
            }
            else {
                creditByBranch.put(entryUnit.branchId, entryUnit.amount.add(creditByBranch.get(entryUnit.branchId)));
            }
        });
        List<GJEntryUnitDTO> interstores = new ArrayList<>();
        debitByBranch.forEach((branchId, totalDebit) -> {
            BigDecimal totalCredit = creditByBranch.get(branchId);
            int cmp = totalCredit.compareTo(totalDebit);
            if (cmp != 0) {
                GJEntryUnitDTO newEntry = new GJEntryUnitDTO();
                newEntry.amount = totalCredit.subtract(totalDebit).abs();
                newEntry.accountCode = InterstoreCode;
                newEntry.setDebit(cmp > 0);
                newEntry.branchId = branchId;
                interstores.add(newEntry);
            }
        });
        return interstores;
    }
}