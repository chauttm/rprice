package com.gem.dto;



import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gem.dto.constant.RentalPriceConst;
import com.gem.model.custom_mapper.PGJsonBObject;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.UUID;

public class RentalPriceDTO extends PGJsonBObject {
    public String className = this.getClass().getName();
    private UUID webId = UUID.randomUUID();
    public BigDecimal rate;
    public String title;
    public Integer meterLimit;

//    for Definition
    public Integer length;  //days
    public TimeUnit timeUnit;

//    For TimeSpan
    public LocalTime startTime;
    public LocalTime endTime;
    public DayOfWeek startDayOfWeek;
    public DayOfWeek endDayOfWeek;
    public int index;
    public UUID parentId;
    public String alias;

    //for algo
    @JsonIgnore
    private String type = null;
    @JsonIgnore
    public String getType()
    {
        if (type != null) return type;

        type = RentalPriceConst.OTHER;
        if (startTime == null && endTime == null && startDayOfWeek == null && endDayOfWeek == null && length != null) {
            if (length == 1 && timeUnit == TimeUnit.Hour) type = RentalPriceConst.HOUR;
            else if (length == 7 && timeUnit == TimeUnit.Day) type = RentalPriceConst.WEEK;
            else if (length == 30 && timeUnit == TimeUnit.Day) type = RentalPriceConst.MONTH;
            else if (length == 1 && timeUnit == TimeUnit.Day) type = RentalPriceConst.DAY;
        }
        else if (length == null && meterLimit == null && !hasStartAndEnd()) type = RentalPriceConst.FLAT;

        return type;
    }

    @JsonIgnore
    public boolean hasTimeConstraints() {
        return ((startDayOfWeek != null && endDayOfWeek != null) || length != null);
    }

    @JsonIgnore
    public Double getLengthInDays() {
        if (startDayOfWeek != null && endDayOfWeek != null) {
            int len = endDayOfWeek.ordinal() - startDayOfWeek.ordinal();
            return 1.0 * (len < 0 ? (len + 7) : (len));
        }
        if (length == null) return null;
        switch (timeUnit) {
            case Day: return length * 1.0;
            case Hour: return length / 24.0;
            case Minute: return length / 144.0;
            default: return null;
        }
    }

    public RentalPriceDTO()
    {
    }


    public RentalPriceDTO(String title, BigDecimal rate) {
        this.title = title;
        this.rate = rate;
    }

    public RentalPriceDTO(String title, BigDecimal rate, Integer meterLimit) {
        this(title, rate);
        this.meterLimit = meterLimit;
    }

    public RentalPriceDTO(String title, BigDecimal rate, Integer meterLimit, Integer length) {
        this(title, rate, meterLimit);
        this.length = length;
        this.timeUnit = TimeUnit.Day;
    }

    public RentalPriceDTO(TimeUnit timeUnit, BigDecimal rate, Integer meterLimit, int length) {
        this(null, rate, meterLimit);
        this.length = length;
        this.timeUnit = timeUnit;
    }


    public RentalPriceDTO(DayOfWeek from, DayOfWeek to, BigDecimal rate, Integer meterLimit) {
        this(null, rate, meterLimit);
        this.length = null;
        this.timeUnit = null;
        this.startDayOfWeek = from;
        this.endDayOfWeek = to;
    }

    public boolean hasStartAndEnd() {
        return startDayOfWeek != null || endDayOfWeek != null || startTime != null || endTime != null;
    }


    public enum  TimeUnit {
        Minute, Hour, Day
    }

    public void setWebId(UUID id) {
        this.webId = id;
    }

    public UUID getWebId() {
        return webId == null ? UUID.randomUUID() : webId;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31)
                .append(title)
                .append(webId)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
        {
            return true;
        }
        if (!(obj instanceof RentalPriceDTO))
        {
            return false;
        }

        RentalPriceDTO other = (RentalPriceDTO) obj;
        return new EqualsBuilder().append(title, other.title)
                .append(webId, other.webId)
                .isEquals();
    }

    public void clone(RentalPriceDTO rentalPriceDTO) {
        this.rate = rentalPriceDTO.rate;
        this.title = rentalPriceDTO.title;
        this.meterLimit = rentalPriceDTO.meterLimit;
        this.length = rentalPriceDTO.length;
        this.timeUnit = rentalPriceDTO.timeUnit;
        this.startTime = rentalPriceDTO.startTime;
        this.startDayOfWeek = rentalPriceDTO.startDayOfWeek;
        this.endTime = rentalPriceDTO.endTime;
        this.endDayOfWeek = rentalPriceDTO.endDayOfWeek;
        this.index = rentalPriceDTO.index;
        this.parentId = rentalPriceDTO.webId;
        this.alias = rentalPriceDTO.alias;
    }

}
