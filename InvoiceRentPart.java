package com.gem.model.common;

import com.gem.dto.InvoiceRentPartDTO;
import com.gem.model.common.enums.RentalTimeUnit;
import com.gem.model.custom_mapper.LocalDateTimeAttributeConverter;
import javafx.util.Pair;
import org.hibernate.annotations.Formula;

import javax.persistence.AccessType;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "inv_rent_part")
@Access(AccessType.FIELD)
public class InvoiceRentPart extends InvoiceLine {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "part_id")
    private Part part;

    @Column(name = "unit_price")
    private BigDecimal unitPrice = BigDecimal.ZERO;

    @Column(name = "quantity")
    private double quantity;

    @Column(name = "num_of_time")
    private double numOfTime;

    @Column(name = "rental_time_unit")
    @Enumerated(EnumType.STRING)
    private RentalTimeUnit rentalTimeUnit = RentalTimeUnit.DAY;

    @Column(name = "start_date")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    LocalDateTime startDate;

    @Column(name = "end_date")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    LocalDateTime endDate;

    @Column(name = "delivery_date")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    LocalDateTime deliveryDate;

    @Column(name = "return_date")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    LocalDateTime returnDate;

    @Formula(value = "case when delivery_date is null then start_date else delivery_date end")
    private LocalDateTime actualStartDate;

    @Formula(value = "case when return_date is null then end_date else return_date end")
    private LocalDateTime actualEndDate;


    public InvoiceRentPart()
    {

    }

    public InvoiceRentPart(QuoteT quote, Part part, InvoiceRentPartDTO invoiceRentPartDTO)
    {
        this.invoice = quote;
        this.dealer = quote.getDealer();
        this.part = part;
        this.webId = (invoiceRentPartDTO.getWebId() != null) ? invoiceRentPartDTO.getWebId() : UUID.randomUUID();
        updateFromDTO(invoiceRentPartDTO);
    }

    public Part getPart() {
        return part;
    }

    public void setPart(Part part) {
        this.part = part;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getNumOfTime() {
        return numOfTime;
    }

    public void setNumOfTime(double numOfTime) {
        this.numOfTime = numOfTime;
    }

    public RentalTimeUnit getRentalTimeUnit() {
        return rentalTimeUnit;
    }

    public void setRentalTimeUnit(RentalTimeUnit rentalTimeUnit) {
        this.rentalTimeUnit = rentalTimeUnit;
    }

    public void updateFromDTO(InvoiceRentPartDTO invoiceRentPartDTO) {
        super.updateFromDTO(invoiceRentPartDTO);
        this.unitPrice = invoiceRentPartDTO.unitPrice;
        this.quantity = invoiceRentPartDTO.quantity;
        this.startDate = invoiceRentPartDTO.startDate;
        this.endDate = invoiceRentPartDTO.endDate;
        this.rentalTimeUnit = invoiceRentPartDTO.rentalTimeUnit;
        this.numOfTime = invoiceRentPartDTO.numOfTime;
    }

    public void doRent(PartBranchDetail partBranchDetail, double qty, LocalDateTime date) {
        partBranchDetail.doRent(qty);
        this.deliveryDate = date;
    }

    public void doReserve(PartBranchDetail partBranchDetail, double diff) {
        partBranchDetail.doReservedRent(diff);
    }

    public void doReturn(PartBranchDetail partBranchDetail, LocalDateTime returnDate) {
        partBranchDetail.doReturn(quantity);
        this.returnDate = returnDate;
    }

    public BigDecimal getSalePrice() {
        return preTaxPrice.add(tax);
    }

    public void cancelReserved(PartBranchDetail partBranchDetail, double qty) {
        partBranchDetail.cancelReservedRent(qty);
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public LocalDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDateTime deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }

    public LocalDateTime getActualStartDate()
    {
        if (deliveryDate == null) return startDate;
        return deliveryDate;
    }

    public LocalDateTime getActualEndDate()
    {
        if (returnDate == null) return endDate;
        return returnDate;
    }

    public boolean isRented()
    {
        return !deleted && !cancel && deliveryDate != null && returnDate == null;
    }

    public boolean isReturned()
    {
        return !deleted && !cancel && returnDate != null;
    }

    public boolean isNotDeliver()
    {
        return !deleted && !cancel && deliveryDate == null;
    }

    public static double getQtyAvailable(LocalDateTime from, LocalDateTime to, double totalQty, List<InvoiceRentPart> rents, LocalDate today) {
        List<Pair<LocalDateTime, Double>> points = new ArrayList<>();
        rents.forEach(rent -> {
            LocalDateTime startPoint = rent.deliveryDate == null ? rent.startDate : rent.deliveryDate;
            LocalDateTime endPoint;
            if (rent.returnDate != null) endPoint = rent.returnDate;
            else if (rent.deliveryDate == null || rent.endDate.toLocalDate().isAfter(today)) {
                endPoint = rent.endDate;
            }
            else endPoint = today.plusDays(1).atStartOfDay().minusSeconds(1);
            if (endPoint.isEqual(startPoint)) endPoint = endPoint.plusSeconds(1);
            System.err.println( startPoint + " " + endPoint + " " + today + " f " + from + " t " + to);

            if (! (endPoint.isBefore(from) || startPoint.isAfter(to))) {
                points.add(new Pair<>(startPoint, rent.getQuantity()));
                points.add(new Pair<>(endPoint, -rent.getQuantity()));
            }
        });
        points.sort(Comparator.comparing(Pair::getKey));
        double rentedQty = 0.0;
        double maxRented = 0.0;
        for (Pair<LocalDateTime, Double> p : points) {
            rentedQty += p.getValue();
            maxRented = Math.max(maxRented, rentedQty);
        }

        return totalQty - maxRented;
    }
}
