package com.gem.model.common;

import org.junit.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InvoiceRentPartTest {
    LocalDate d1 = LocalDate.of(2023, 02, 01);
    LocalDate d2 = LocalDate.of(2023, 02, 02);
    LocalDate d10 = LocalDate.of(2023, 02, 10);
    LocalDate d11 = LocalDate.of(2023, 02, 11);
    LocalDate today = LocalDate.of(2023, 01,01);

    @Test
    public void testGetQtyAvailable_oneRent() {
        InvoiceRentPart rent1 = new InvoiceRentPart();
        rent1.setStartDate(d2.atStartOfDay());
        rent1.setEndDate(d10.atStartOfDay());
        rent1.setQuantity(1);
        List<InvoiceRentPart> rents = Arrays.asList(rent1);

        assertEquals("cover", 10-1, InvoiceRentPart.getQtyAvailable(d1.atStartOfDay(), d11.atStartOfDay(),10, rents, today), 0.01);

        assertEquals("cover first half", 10-1, InvoiceRentPart.getQtyAvailable(d1.atStartOfDay(), rent1.getEndDate().minusMinutes(1),10, rents, today), 0.01);

        assertEquals("cover second half", 10-1, InvoiceRentPart.getQtyAvailable(rent1.getStartDate().plusMinutes(1), d11.atStartOfDay(),10, rents, today), 0.01);

        assertEquals("before", 10, InvoiceRentPart.getQtyAvailable(d1.atStartOfDay(), rent1.getStartDate().minusMinutes(1),10, rents, today), 0.01);

        assertEquals("after", 10, InvoiceRentPart.getQtyAvailable(rent1.getEndDate().plusMinutes(1), d11.atStartOfDay(),10, rents, today), 0.01);
    }

    @Test
    public void testGetQtyAvailable_twoRents_disjoint() {
        InvoiceRentPart rent1 = new InvoiceRentPart();
        rent1.setStartDate(d2.atStartOfDay());
        rent1.setEndDate(d10.atStartOfDay());
        rent1.setQuantity(1);

        InvoiceRentPart rent2 = new InvoiceRentPart();
        rent2.setStartDate(rent1.getEndDate().plusHours(1));
        rent2.setEndDate(d11.atStartOfDay());
        rent2.setQuantity(2);

        List<InvoiceRentPart> rents = Arrays.asList(rent1, rent2);

        assertEquals("cover all", 10-2, InvoiceRentPart.getQtyAvailable(rent1.getStartDate(), rent2.getEndDate(),
                10, rents, today), 0.01);

        assertEquals("in between", 10, InvoiceRentPart.getQtyAvailable(rent1.getEndDate().plusMinutes(1), rent2.getStartDate().minusMinutes(1),
                10, rents, today), 0.01);

        assertEquals("before", 10, InvoiceRentPart.getQtyAvailable(d1.atStartOfDay(), rent1.getStartDate().minusMinutes(1),
                10, rents, today), 0.01);

        assertEquals("after", 10, InvoiceRentPart.getQtyAvailable(rent2.getEndDate().plusMinutes(1), d11.atStartOfDay(),
                10, rents, today), 0.01);
    }

    @Test
    public void testGetQtyAvailable_twoRents_overlapping() {
        InvoiceRentPart rent1 = new InvoiceRentPart();
        rent1.setStartDate(d2.atStartOfDay());
        rent1.setEndDate(d10.atStartOfDay());
        rent1.setQuantity(1);

        InvoiceRentPart rent2 = new InvoiceRentPart();
        rent2.setStartDate(rent1.getEndDate().minusDays(1));
        rent2.setEndDate(d11.atStartOfDay());
        rent2.setQuantity(2);

        List<InvoiceRentPart> rents = Arrays.asList(rent1, rent2);

        assertEquals("cover all", 10-1-2, InvoiceRentPart.getQtyAvailable(rent1.getStartDate(), rent2.getEndDate(),
                10, rents, today), 0.01);

        assertEquals("cover first", 10-1, InvoiceRentPart.getQtyAvailable(rent1.getStartDate(), rent2.getStartDate().minusMinutes(1),
                10, rents, today), 0.01);

        assertEquals("cover second", 10-2, InvoiceRentPart.getQtyAvailable(rent1.getEndDate().plusMinutes(1), rent2.getEndDate(),
                10, rents, today), 0.01);
    }

    @Test
    public void testNotYetReturn_Late() {
        InvoiceRentPart rent1 = new InvoiceRentPart();
        rent1.setStartDate(d2.atStartOfDay());
        rent1.setDeliveryDate(d2.atStartOfDay().plusHours(1));
        rent1.setEndDate(d10.atStartOfDay());
        rent1.setQuantity(1);

        List<InvoiceRentPart> rents = Arrays.asList(rent1);

        assertEquals("reserve future rent", 10, InvoiceRentPart.getQtyAvailable(d11.plusDays(1).atStartOfDay(), d11.plusDays(2).atStartOfDay(),
                10, rents, d11), 0.01);

        assertEquals("reserve for today", 10-1, InvoiceRentPart.getQtyAvailable(d11.atStartOfDay(), d11.plusDays(2).atStartOfDay(),
                10, rents, d11), 0.01);
    }

    @Test
    public void testNotYetReturn_NotDueYet() {
        InvoiceRentPart rent1 = new InvoiceRentPart();
        rent1.setStartDate(d2.atStartOfDay());
        rent1.setDeliveryDate(d2.atStartOfDay().plusHours(1));
        rent1.setEndDate(d10.atStartOfDay());
        rent1.setQuantity(1);

        List<InvoiceRentPart> rents = Arrays.asList(rent1);

        assertEquals("reserve future rent", 10, InvoiceRentPart.getQtyAvailable(d11.plusDays(1).atStartOfDay(), d11.plusDays(2).atStartOfDay(),
                10, rents, d10.minusDays(1)), 0.01);

        assertEquals("reserve for today", 10, InvoiceRentPart.getQtyAvailable(d11.atStartOfDay(), d11.plusDays(2).atStartOfDay(),
                10, rents, d10.minusDays(1)), 0.01);
    }

    @Test
    public void testLateReturn() {
        InvoiceRentPart rent1 = new InvoiceRentPart();
        rent1.setStartDate(d2.atStartOfDay());
        rent1.setDeliveryDate(d2.atStartOfDay().plusHours(1));
        rent1.setEndDate(d10.atStartOfDay());
        rent1.setReturnDate(d11.atStartOfDay());
        rent1.setQuantity(1);

        List<InvoiceRentPart> rents = Arrays.asList(rent1);

        assertEquals("not overlap", 10, InvoiceRentPart.getQtyAvailable(d11.atStartOfDay().plusMinutes(1), d11.plusDays(2).atStartOfDay(),
                10, rents, d11), 0.01);

        assertEquals("overlap", 10-1, InvoiceRentPart.getQtyAvailable(d10.atStartOfDay().plusHours(1), d11.plusDays(2).atStartOfDay(),
                10, rents, d11), 0.01);
    }
}
