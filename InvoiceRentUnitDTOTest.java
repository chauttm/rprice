package com.gem.dto;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class InvoiceRentUnitDTOTest {
    InvoiceRentUnitDTO dto = new InvoiceRentUnitDTO();
    RentalPriceQtyDTO dayRate = new RentalPriceQtyDTO(RentalPriceDTO.DAY, BigDecimal.valueOf(225), 8, 1);
    RentalPriceQtyDTO weeklyRate = new RentalPriceQtyDTO(RentalPriceDTO.WEEK, BigDecimal.valueOf(900), 40, 7);
    RentalPriceQtyDTO monthlyRate = new RentalPriceQtyDTO(RentalPriceDTO.MONTH, BigDecimal.valueOf(2700), 160, 30);
    RentalPriceQtyDTO hourlyRate = new RentalPriceQtyDTO(RentalPriceDTO.HOUR, BigDecimal.valueOf(40), 10, 0);

    double totalContractedBeforeDiscount;

    @Before
    public void setUp() {
        dto.rentalPriceQtyDTOs = Arrays.asList(dayRate, weeklyRate, monthlyRate);

        dto.extPrice = BigDecimal.valueOf(3881.25);
        dto.preTaxPrice = BigDecimal.valueOf(3881.25);

        dto.includeVat = false;
        dto.percentTax = 10.0;

        dayRate.qty = 2.0;
        monthlyRate.qty = 1.0;

        totalContractedBeforeDiscount = 2700 + 2*225;
        dto.onUpdateRates(2);

        assertEquals(totalContractedBeforeDiscount, dto.totalContracted.doubleValue(), 0.01);
        assertEquals(0, dto.discountContracted.doubleValue(), 0.01);

        assertEquals(0, dto.totalOvercharge.doubleValue(), 0.01);
        assertEquals(0, dto.discountOvercharge.doubleValue(), 0.01);

        assertEquals(totalContractedBeforeDiscount, dto.extPrice.doubleValue(), 0.01);
        assertEquals(0, dto.discount.doubleValue(), 0.01);
    }

    @Test
    public void testOnUpdateRates() {
        dto.onUpdateRates(2);
    }

    @Test
    public void testOnUpdateDiscountContracted() {
        dto.discountContracted = BigDecimal.TEN;
        dto.onUpdateDiscountContracted(2);
        assertEquals(totalContractedBeforeDiscount - 10, dto.totalContracted.doubleValue(), 0.01);
        assertEquals(10, dto.discountContracted.doubleValue(), 0.01);

        assertEquals(0, dto.totalOvercharge.doubleValue(), 0.01);
        assertEquals(0, dto.discountOvercharge.doubleValue(), 0.01);

        assertEquals(totalContractedBeforeDiscount - 10, dto.extPrice.doubleValue(), 0.01);
        assertEquals(10, dto.discount.doubleValue(), 0.01);
    }

    @Test
    public void testOnUpdateTotalContracted() {
        dto.totalContracted = BigDecimal.valueOf(3400);
        dto.onUpdateTotalContracted(2);
        assertEquals(3400, dto.totalContracted.doubleValue(), 0.01);
        assertEquals(totalContractedBeforeDiscount - 3400, dto.discountContracted.doubleValue(), 0.01);

        assertEquals(0, dto.totalOvercharge.doubleValue(), 0.01);
        assertEquals(0, dto.discountOvercharge.doubleValue(), 0.01);

        assertEquals(3400, dto.extPrice.doubleValue(), 0.01);
        assertEquals(totalContractedBeforeDiscount - 3400, dto.discount.doubleValue(), 0.01);
    }

    @Test
    public void testOnUpdateOvertime() {
        dto.overTime = 10.0;
        dto.overchargeRate = BigDecimal.valueOf(40);
        dto.onUpdateOvertime(2);
        assertEquals(totalContractedBeforeDiscount, dto.totalContracted.doubleValue(), 0.01);
        assertEquals(0, dto.discountContracted.doubleValue(), 0.01);

        assertEquals(40 *10, dto.totalOvercharge.doubleValue(), 0.01);
        assertEquals(0, dto.discountOvercharge.doubleValue(), 0.01);

        assertEquals(totalContractedBeforeDiscount + 400, dto.extPrice.doubleValue(), 0.01);
        assertEquals(0, dto.discount.doubleValue(), 0.01);
    }

    @Test
    public void testOnUpdateTotalOvercharge() {
        dto.overchargeRate = BigDecimal.valueOf(40);
        dto.overTime = 10.0;
        dto.totalOvercharge = BigDecimal.valueOf(300);
        dto.onUpdateTotalOvercharge(2);
        assertEquals(totalContractedBeforeDiscount, dto.totalContracted.doubleValue(), 0.01);
        assertEquals(0, dto.discountContracted.doubleValue(), 0.01);

        assertEquals(300, dto.totalOvercharge.doubleValue(), 0.01);
        assertEquals(40*10 - 300, dto.discountOvercharge.doubleValue(), 0.01);

        assertEquals(totalContractedBeforeDiscount + 300, dto.extPrice.doubleValue(), 0.01);
        assertEquals(40* 10 - 300, dto.discount.doubleValue(), 0.01);
    }
}
